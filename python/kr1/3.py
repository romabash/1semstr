import time 




def SAXPY (x,y,a):
    return  [x_*a+y_ for x_,y_  in zip (x,y)]

import timeit

x = []
y = []

raz=100000
f = open("time_kr_3.txt", 'w')
for iteration in range(10):
    raz+=100000
    for ras in range(raz):
        x.append(ras)
        y.append(ras)
    elapsed=0
    for i in range (3):
        start_time = timeit.default_timer()
        z = SAXPY(x,y,i)
        elapsed += timeit.default_timer() - start_time
    elapsed /= 3
    f.writelines(f"{elapsed:f}\n")
    print(elapsed)
f.close()