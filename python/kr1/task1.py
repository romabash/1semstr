import sys 
resul = 42.0

while True:
    try:
        line = input()
        print (line)
        resul+=1/(float(line))
    except EOFError: 
        print("Ввод окочен")
        break
    except ZeroDivisionError:
        print("Деление на ноль")
        break
    except TypeError:
        print("Ошибка данных")
        break

print(f"Cумма ряда {resul}")
