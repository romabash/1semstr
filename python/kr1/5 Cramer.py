import time
import sys


def MatixGenerator(ras):
    m = []
    for i in range(ras):
        m.append([])
        for j in range(ras+1):
            m[i].append((j+1)**(i+1))

    return m


def SlochenieStrok(a, b):
    for i in range(len(a)):
        a[i] = a[i]+b[i]
    return a[:]


def UmnStrNaK(row, k):
    for i in range(len(row)):
        row[i] = row[i]*k
    return row[:]


def matrixDet(m):  # определитель матрицы по гауссу
    n = len(m)
    # Зануление нижнего левого угла теугольный вид
    for k in range(n):
        for row in range(k + 1, n):
            m[row] = SlochenieStrok(
                m[row][:], UmnStrNaK(m[k][:], -m[row][k]/m[k][k]))
    det = 1
    for i in range(n):
        det *= m[i][i]
    return det


def xMas(mas, col):  # столбец свободных членов ставится за место указанного
    size = len(mas)
    masBuf = []
    for i in range(size):
        masBuf.append([])
        for j in range(size):
            if (j == col):
                masBuf[i].append(mas[i][size])
            else:
                masBuf[i].append(mas[i][j])
    return masBuf


def Cramer(mas):
    size = len(mas)
    det = matrixDet(mas[:][:size])  # главный определитель системы
    if (det == 0):
        print("Cистема имеет бесконечно много решений или несовместна (не имеет решений).")
    else:
        x = []
        for i in range(size):
            m = xMas(mas, i)
            xDet = round(matrixDet(m), 13)
            x.append(xDet/det)
            # print(f"x[{i}]={x[i]}")
# ВВОД
# mas= []
# s =int (input("Введите количество неизвесных"))
# for i in range(s):
#     mas.append([])
#     for j in range(s+1):
#         if (j ==s):
#             mas[i].append( f"s{i}")
#         else:
#             mas[i].append( f"a[{i},{j}]*x[{j}]")

# for i in range(s):
#     for j in range(s+1):
#         if (j ==s):
#             print('=',end='')
#         print(mas[i][j],end='')
#         if (j<s-1):
#             print('+',end='')
#     print()

# for i in range(s):
#     for j in range(s+1):
#         if (j ==s):
#             mas[i][j] = float (input (f"Введите s{i} "))
#         else:
#             mas[i][j] = float (input (f"Введите a[{i},{j}] "))
# m=mas[:]

# for i in range(s):
#     for j in range(s+1):
#         if (j ==s):
#             print('=',end='')
#         print(mas[i][j],end='')
#         if (j<s-1):
#             print('+',end='')


# mas = [ [1,2, 3,14],
#         [1,1, 1, 6],
#         [1,2,-1, 2] ]

f = open("CramerTimeMarix.txt", 'w')
f_mem = open("CramerMemoruMarix.txt", 'w')
for ras in range(10, 60, 2):
    m = MatixGenerator(ras)
    start_time = time.time()
    Cramer(m)
    elapsed = time.time() - start_time
    f.writelines(f"{elapsed:f}\n")
    f_mem.writelines(f"{sys.getsizeof(m):f}\n")  # запись в памяти
    print(sys.getsizeof(m))
f.close()
f_mem.close()
