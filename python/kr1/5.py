import sys
import time
import random


def SlochenieStrok(a, b):
    for i in range(len(a)):
        a[i] = a[i]+b[i]
    return a[:]


def UmnStrNaK(row, k):
    for i in range(len(row)):
        row[i] = row[i]*k
    return row[:]


def GausJordan(m):  # обработать 0
    n = len(m)
    # Прямой ход (диагональ едениц и  Зануление нижнего левого угла)
    for k in range(n):
        # диагональ едениц
        if m[k][k] != 1:
            m[k][:] = UmnStrNaK(m[k][:], 1 / m[k][k])
        # Зануление нижнего левого угла
        for row in range(k + 1, n):
            m[row] = SlochenieStrok(m[row][:], UmnStrNaK(m[k][:], -m[row][k]))

    # Обратный ход (Зануление верхнего правого угла)
    for k in range(n - 1, 0, -1):
        for row in range(k - 1, -1, -1):
            if m[row][k] != 0:
                # Зануление верхнего правого угла
                m[row] = SlochenieStrok(
                    m[row][:], UmnStrNaK(m[k][:], -m[row][k]))
    # Вывод-------------------------------------------
    if(False):
        print()
        for e in m:
            print(e)
        print()
        for e in m:
            print(e)
        print()
        x = []
        for i in range(n):
            print(f"x{i}={m[i][n]}")
            x.append(m[i][n])
        return x
    # Вывод-------------------------------------------


# def MatixGenerator(ras):
#     m = []
#     for i in range(ras):
#         m.append([])
#         for j in range(ras+1):
#             if(i == 0):
#                 m[i].append(1)
#             else:
#                 if (j == 0):
#                     m[i].append(1)
#                 elif (j == 1):
#                     m[i].append(i+1)
#                 else:
#                     m[i].append(m[i][j-1]*i)
#     return m
def MatixGenerator(ras):
    m = []
    for i in range(ras):
        m.append([])
        for j in range(ras+1):
            m[i].append((j+1)**(i+1))
    return m
# вввод------------------------------------------------
# mas= []
# s =int (input("Введите количество неизвесных"))
# for i in range(s):
#     mas.append([])
#     for j in range(s+1):
#         if (j ==s):
#             mas[i].append( f"s{i}")
#         else:
#             mas[i].append( f"a[{i},{j}]*x[{j}]")
# for i in range(s):
#     for j in range(s+1):
#         if (j ==s):
#             print('=',end='')
#         print(mas[i][j],end='')
#         if (j<s-1):
#             print('+',end='')
#     print()
# for i in range(s):
#     for j in range(s+1):
#         if (j ==s):
#             mas[i][j] = float (input (f"Введите s{i} "))
#         else:
#             mas[i][j] = float (input (f"Введите a[{i},{j}] "))
# m=mas[:]
# -----------------------------------------------------------


f = open("TimeMarix.txt", 'w')
f_mem = open("MemoruMarix.txt", 'w')
for ras in range(10, 60, 2):
    # m =[[8,3,4,5,31],
    # [14,4,33,23,17],
    # [15,4,23,7,22],
    # [4,11,17,1,51]]
    # x0=1.804049981919725
    # x1=6.307927196753582
    # x2=-1.5513680742496692
    # x3=0.7698581702760261
    m = MatixGenerator(ras)
    start_time = time.time()
    GausJordan(m)
    elapsed = time.time() - start_time
    f.writelines(f"{elapsed:f}\n")
    f_mem.writelines(f"{sys.getsizeof(m):f}\n")  # запись в памяти
    print(sys.getsizeof(m))
f.close()
f_mem.close()
